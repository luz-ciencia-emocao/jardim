ArrayList<Jardim> jardim = new ArrayList<Jardim>();

Controlador cmaster;

final int diam = 350;
final float dmini = 0.05*diam;
final int delayms = 100;

final int NLED = 3+3*2*8; // 3 LEDs centrais + 3 jardins de 2 aneis com 8 LEDs = 51 LEDs
color[] arrayColor = new color[NLED];

color fromColor = #FF0000;
color toColor = #FF0000;

void setup() {
  size(1200, 600);

  smooth();
  noStroke();

  cmaster = new Controlador(true, 3);

  for (int i = 0; i < 3; ++i) {
    jardim.add(new Jardim((i+0.5)*width/3f, height/2f, diam));
  }
}

void draw() {
  background(255);

  if (isTime()) {
    updateColors();
  }

  for (Jardim j : jardim) {
    j.display();
  }

  diplayCentral(cmaster.cor);
}

void diplayCentral(color[] c) {
  if (c.length < 3)
    return;

  for (int i = 0; i < 3; ++i) {
    fill(c[i]);
    ellipse((i+0.5)*width/3f, height/2f, dmini, dmini);
  }
}

int lastMillis = 0;
boolean isTime() {
  if (millis() - lastMillis >= delayms) {
    lastMillis = millis();
    return true;
  }
  return false;
}

void updateColors() {
  for (int i = 0; i < cmaster.cor.length; ++i) {
    cmaster.cor[i] = lerpColor(fromColor, toColor, i/float(cmaster.cor.length));
  }
  //for (int i = 0; i < cmaster.cor.length; ++i) {
  //if (cmaster.cor[i] == color(0)) {
  //cmaster.cor[i] = color(255);
  //} else {
  //  cmaster.cor[i] = color(0);
  //}
  //}
}

/*
1-9
2-10
*/