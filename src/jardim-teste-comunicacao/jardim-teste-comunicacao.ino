// pinos
#define LED_PIN   A0  // led uso geral
#define MAX_EN    4   // des/habilita transmissao MAX485
// #define MODE_PIN  12  // modo de operacao
// #define SS_PIN    10  // slave select
// #define SCK_PIN   13  // clock
// #define MOSI_PIN  11  // mosi

// placas
enum {
  MASTER = 0,
  JARDIM_ESQ_INT,
  JARDIM_ESQ_EXT,
  JARDIM_CEN_INT,
  JARDIM_CEN_EXT,
  JARDIM_DIR_INT,
  JARDIM_DIR_EXT,
  ADDRESS_MAX
} address;

// comandos
const byte ACENDER = 123;
const byte APAGAR = 250;

void setup() {
  // placa
  address = MASTER;

  // serial
  Serial.begin(9600);
  // pinos
  pinMode(LED_PIN, OUTPUT);
  pinMode(MAX_EN, OUTPUT);
  // apaga o LED
  digitalWrite(LED_PIN, LOW);

  // master: HIGH
  // outros: LOW
  boolean mode = (address==MASTER)?HIGH:LOW;
  digitalWrite(MAX_EN, mode);
}

void loop() {
  if(address == MASTER){
      Serial.write(ACENDER);
      digitalWrite(LED_PIN, HIGH);
      delay(1000);
      
      Serial.write(APAGAR);
      digitalWrite(LED_PIN, LOW);
      delay(1000);
  }else{
    if(Serial.available() > 0){
        byte recv = Serial.read();
        if(recv == ACENDER){
          digitalWrite(LED_PIN, HIGH);
        }else if(recv == APAGAR){
          digitalWrite(LED_PIN, LOW);
        }
    }
  }
}
